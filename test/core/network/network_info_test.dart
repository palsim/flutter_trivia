import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_trivia/core/network/network_info.dart';
import 'package:mocktail/mocktail.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:data_connection_checker/data_connection_checker.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

void main() {
  late NetworkInfoImpl networkInfoImpl;
  late MockDataConnectionChecker mockDataConnectionChecker;

  setUp(() {
    mockDataConnectionChecker = MockDataConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockDataConnectionChecker);
  });

  group('isConnected', () {
    test('should forward the call to DataConnectionChecker.hasConnection',
        () async {
      //arrange
      final tHasConnectionFuture = Future.value(true);
      when(() => mockDataConnectionChecker.hasConnection)
          .thenAnswer((_) async => tHasConnectionFuture);

      //act
      final result = networkInfoImpl.isConnected;

      //assert
      verify(() => mockDataConnectionChecker.hasConnection);
      expect(await result, await tHasConnectionFuture);
    });
  });
}
