import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_trivia/core/error/exceptions.dart';
import 'package:flutter_trivia/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:flutter_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart' as http;

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  late NumberTriviaRemoteDataSourceImpl dataSource;
  late MockHttpClient mockHttpClient;

  const int tNumber = 1;
  final tNumberTriviaModel =
      NumberTriviaModel.fromJson(json.decode(fixture("trivia.json")));
  final Uri uriNumber = Uri.parse("http://numbersapi.com/$tNumber");
  final Uri uriRandom = Uri.parse("http://numbersapi.com/random");

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = NumberTriviaRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200(Uri uri) {
    when(() => mockHttpClient.get(uri, headers: any(named: "headers")))
        .thenAnswer((_) async => http.Response(fixture("trivia.json"), 200));
  }

  void setUpMockHttpClientFailure404(Uri uri) {
    when(() => mockHttpClient.get(uri, headers: any(named: "headers")))
        .thenAnswer((_) async => http.Response("Something went wrong", 404));
  }

  group('getConcreteNumberTrivia', () {
    test('''should performe a GET request on a URL with 
            number being the endpoint and with application/json header''',
        () async {
      //arrange
      setUpMockHttpClientSuccess200(uriNumber);

      //act
      await dataSource.getConcreteNumberTrivia(tNumber);

      //assert
      verify(() => mockHttpClient.get(
            uriNumber,
            headers: {"Content-Type": "application/json"},
          ));
    });

    test(
        'should return NumberTriviaModel when the response code is 200 (success)',
        () async {
      //arrange
      setUpMockHttpClientSuccess200(uriNumber);

      //act
      final result = await dataSource.getConcreteNumberTrivia(tNumber);

      //assert
      expect(result, equals(tNumberTriviaModel));
    });

    test(
        'should throw a ServerException when the response code is 404 or other',
        () async {
      //arrange
      setUpMockHttpClientFailure404(uriNumber);

      //act
      final call = dataSource.getConcreteNumberTrivia;

      //assert
      expect(
          () => call(tNumber), throwsA(const TypeMatcher<ServerException>()));
    });
  });

  group('getRandomNumberTrivia', () {
    test('''should performe a GET request on a URL with 
            random being the endpoint and with application/json header''',
        () async {
      //arrange
      setUpMockHttpClientSuccess200(uriRandom);

      //act
      await dataSource.getRandomNumberTrivia();

      //assert
      verify(() => mockHttpClient.get(
            uriRandom,
            headers: {"Content-Type": "application/json"},
          ));
    });

    test(
        'should return NumberTriviaModel when the response code is 200 (success)',
        () async {
      //arrange
      setUpMockHttpClientSuccess200(uriRandom);

      //act
      final result = await dataSource.getRandomNumberTrivia();

      //assert
      expect(result, equals(tNumberTriviaModel));
    });

    test(
        'should throw a ServerException when the response code is 404 or other',
        () async {
      //arrange
      setUpMockHttpClientFailure404(uriRandom);

      //act
      final call = dataSource.getRandomNumberTrivia;

      //assert
      expect(() => call(), throwsA(const TypeMatcher<ServerException>()));
    });
  });
}
