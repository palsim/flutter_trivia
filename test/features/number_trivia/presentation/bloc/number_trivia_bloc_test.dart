import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_trivia/core/error/failures.dart';
import 'package:flutter_trivia/core/usecases/usecase.dart';
import 'package:flutter_trivia/core/util/input_converter.dart';
import 'package:flutter_trivia/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:flutter_trivia/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:flutter_trivia/features/number_trivia/domain/usecases/get_random_number_trivia.dart';
import 'package:flutter_trivia/features/number_trivia/presentation/bloc/number_trivia_bloc.dart';
import 'package:mocktail/mocktail.dart';

class MockGetConcreteNumberTrivia extends Mock
    implements GetConcreteNumberTrivia {}

class MockGetRandomNumberTrivia extends Mock implements GetRandomNumberTrivia {}

class MockInputConverter extends Mock implements InputConverter {}

void main() {
  late NumberTriviaBloc bloc;
  late MockGetConcreteNumberTrivia mockGetConcreteNumberTrivia;
  late MockGetRandomNumberTrivia mockGetRandomNumberTrivia;
  late MockInputConverter mockInputConverter;

  setUp(() {
    mockGetConcreteNumberTrivia = MockGetConcreteNumberTrivia();
    mockGetRandomNumberTrivia = MockGetRandomNumberTrivia();
    mockInputConverter = MockInputConverter();
    bloc = NumberTriviaBloc(
        concrete: mockGetConcreteNumberTrivia,
        random: mockGetRandomNumberTrivia,
        inputConverter: mockInputConverter);
  });

  test('initialState should be Empty', () {
    //assert
    expect(bloc.state, equals(Empty()));
  });

  group('GetTriviaForConcreteNumber', () {
    const tNumberString = '1';
    const tNumberParsed = 1;
    const tNumberTrivia = NumberTrivia(number: 1, text: 'test trivia');

    test(
        'should call the InputConverter to validate and convert the string to an unsigned integer',
        () async {
      //arrange
      when(() => mockInputConverter.stringToUnsignedInteger(any()))
          .thenReturn(const Right(tNumberParsed));
      when(() =>
              mockGetConcreteNumberTrivia(const Params(number: tNumberParsed)))
          .thenAnswer((_) async => const Right(tNumberTrivia));

      //act
      bloc.add(const GetTriviaForConcreteNumber(tNumberString));
      await untilCalled(
          () => mockInputConverter.stringToUnsignedInteger(any()));

      //assert
      verify(() => mockInputConverter.stringToUnsignedInteger(tNumberString));
    });

    blocTest(
      'should emit [Error] when the input is invalid',
      setUp: () {
        when(() => mockInputConverter.stringToUnsignedInteger(any()))
            .thenReturn(Left(InvalidInputFailure()));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) =>
          bloc.add(const GetTriviaForConcreteNumber(tNumberString)),
      expect: () => [
        const Error(message: invalidInputFailureMessage),
      ],
    );

    blocTest('should get data from the concrete use case',
        setUp: () {
          when(() => mockInputConverter.stringToUnsignedInteger(any()))
              .thenReturn(const Right(tNumberParsed));
          when(() => mockGetConcreteNumberTrivia(
                  const Params(number: tNumberParsed)))
              .thenAnswer((_) async => const Right(tNumberTrivia));
        },
        build: () => bloc,
        act: (NumberTriviaBloc bloc) =>
            bloc.add(const GetTriviaForConcreteNumber(tNumberString)),
        // verify: (bloc) =>
        //     mockGetConcreteNumberTrivia(const Params(number: tNumberParsed)),
        verify: (_) {
          verify(() =>
              mockGetConcreteNumberTrivia(const Params(number: tNumberParsed)));
        });

    blocTest(
      'should emit [Loading, Loaded] when data is gotten successfully',
      setUp: () {
        when(() => mockInputConverter.stringToUnsignedInteger(any()))
            .thenReturn(const Right(tNumberParsed));
        when(() => mockGetConcreteNumberTrivia(
                const Params(number: tNumberParsed)))
            .thenAnswer((_) async => const Right(tNumberTrivia));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) =>
          bloc.add(const GetTriviaForConcreteNumber(tNumberString)),
      expect: () => [Loading(), const Loaded(trivia: tNumberTrivia)],
    );

    blocTest(
      'should emit [Loading, Error] when getting data fails',
      setUp: () {
        when(() => mockInputConverter.stringToUnsignedInteger(any()))
            .thenReturn(const Right(tNumberParsed));
        when(() => mockGetConcreteNumberTrivia(
                const Params(number: tNumberParsed)))
            .thenAnswer((_) async => Left(ServerFailure()));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) =>
          bloc.add(const GetTriviaForConcreteNumber(tNumberString)),
      expect: () => [Loading(), const Error(message: serverFailureMessage)],
    );

    blocTest(
      'should emit [Loading, Error] with proper message for the error when getting data fails',
      setUp: () {
        when(() => mockInputConverter.stringToUnsignedInteger(any()))
            .thenReturn(const Right(tNumberParsed));
        when(() => mockGetConcreteNumberTrivia(
                const Params(number: tNumberParsed)))
            .thenAnswer((_) async => Left(CacheFailure()));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) =>
          bloc.add(const GetTriviaForConcreteNumber(tNumberString)),
      expect: () => [Loading(), const Error(message: cacheFailureMessage)],
    );
  });

  //===================================================================

  group('GetTriviaForRandomNumber', () {
    // const tNumberString = '1';
    // const tNumberParsed = 1;
    const tNumberTrivia = NumberTrivia(number: 1, text: 'test trivia');

    blocTest('should get data from the random use case',
        setUp: () {
          when(() => mockGetRandomNumberTrivia(const NoParams()))
              .thenAnswer((_) async => const Right(tNumberTrivia));
        },
        build: () => bloc,
        act: (NumberTriviaBloc bloc) => bloc.add(GetTriviaForRandomNumber()),
        //verify: (bloc) => mockGetRandomNumberTrivia(const NoParams()),
        verify: (_) {
          verify(() => mockGetRandomNumberTrivia(const NoParams()));
        });

    blocTest(
      'should emit [Loading, Loaded] when data is gotten successfully',
      setUp: () {
        when(() => mockGetRandomNumberTrivia(const NoParams()))
            .thenAnswer((_) async => const Right(tNumberTrivia));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) => bloc.add(GetTriviaForRandomNumber()),
      expect: () => [Loading(), const Loaded(trivia: tNumberTrivia)],
    );

    blocTest(
      'should emit [Loading, Error] when getting data fails',
      setUp: () {
        when(() => mockGetRandomNumberTrivia(const NoParams()))
            .thenAnswer((_) async => Left(ServerFailure()));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) => bloc.add(GetTriviaForRandomNumber()),
      expect: () => [Loading(), const Error(message: serverFailureMessage)],
    );

    blocTest(
      'should emit [Loading, Error] with proper message for the error when getting data fails',
      setUp: () {
        when(() => mockGetRandomNumberTrivia(const NoParams()))
            .thenAnswer((_) async => Left(CacheFailure()));
      },
      build: () => bloc,
      act: (NumberTriviaBloc bloc) => bloc.add(GetTriviaForRandomNumber()),
      expect: () => [Loading(), const Error(message: cacheFailureMessage)],
    );
  });
}
